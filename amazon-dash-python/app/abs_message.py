'''
Created on 2018/06/12

@author: naoki_okada

メッセージの抽象クラス
'''

from abc import ABCMeta,abstractclassmethod

class AbsMessage(metaclass=ABCMeta):

    ''' データを送る '''
    @abstractclassmethod
    def send(self):
        pass

    ''' 宛先を指定する '''
    @abstractclassmethod
    def set_to(self):
        pass

    ''' データ中身を指定する '''
    @abstractclassmethod
    def set_body(self):
        pass

    ''' データ配信元を指定する '''
    @abstractclassmethod
    def set_from(self):
        pass