'''
Created on 2018/06/06

@author: naoki_okada

iniファイルクラス
'''

import configparser
import sys

class ConfigIni():
    def __init__(self):
        self._ini_file = None
        self._encoding = 'UTF-8'
        self._config   = None

    '''  ini_fileのプロパティ  '''
    def _get_ini_file(self):
        return self._ini_file

    def _set_ini_file(self, value):
        self._ini_file = value

    def _del_ini_file(self):
        del self._ini_file

    ini_file = property(_get_ini_file, _set_ini_file, _del_ini_file,
                        "property ini file")

    '''  encodingのプロパティ  '''
    def _get_encoding(self):
        return self._encoding

    def _set_encoding(self, value):
        self._encoding = value

    def _del_encoding(self):
        del self._encoding

    encoding = property(_get_encoding, _set_encoding,
                        _del_encoding,"property encoding")

    def create_configparser(self):
        self._config = configparser.ConfigParser()
        self._config.read(self._ini_file, self._encoding)

    def get_config_value_bykey(self, section, key):
        if(not self._config):
            self.create_configparser()
        return self._config.get(section, key)
