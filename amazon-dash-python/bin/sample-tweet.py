#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os, json, configparser, sys
from requests_oauthlib import OAuth1Session

if __name__ == '__main__':
    INI_FILE = '../config/twitter.ini';
    pathname = os.path.dirname(os.path.abspath(__name__))
    pathname = os.path.join(pathname, INI_FILE)
    pathname = os.path.normpath(pathname)
    config = configparser.ConfigParser()
    config.read(pathname, 'UTF-8')

    CK  = config.get('settings', 'ConsumerKey')
    CS  = config.get('settings', 'ConsumerSecret')
    AT  = config.get('settings', 'AccessToken')
    ATS = config.get('settings', 'AccessTokenSecret')
    url = config.get('settings', 'ApiUrl')
    #print(ATS)
    #sys.exit()

    #ツイート本文
    params = {"status": "Hello, World!"}

    twitter = OAuth1Session(CK, CS, AT, ATS)
    print(twitter.__class__)
    sys.exit()
    req = twitter.post(url, params = params)
    # レスポンスを確認
    if req.status_code == 200:
        print ("OK")
    else:
        print ("Error: %d" % req.status_code)