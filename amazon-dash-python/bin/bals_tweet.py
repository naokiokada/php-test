#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os,sys
from datetime import datetime

TWEET = '''バルス！！！
{countstr}
#バルス #神戸電子専門学校 #Webエンジニアコース #体験授業
'''

'''  ボタンが押されたログを記録する
     この処理を作ります。
'''
def write_log_file(log_file, now_time):
    pass

'''  ボタンの押されたログをカウントする
     この処理を作ります。
'''
def log_file_count(log_file):
    count  = 0
    countstr = ""
    #ここに処理を記入する

    #ここまで処理を記入する

    countstr = str(count)+"回目。dashボタンの名前：バッテリー"

    return countstr

if __name__ == '__main__':
    '''  appの読み込み  '''
    path = os.path.normpath(os.path.join(
    os.path.dirname(os.path.abspath(__file__)),'../'))
    sys.path.append(path)
    from app import common
    from app import msg_simple_factory as factory
    from app import config_ini

    '''  共通情報の取得  '''
    cm = common.Common()
    pathname = cm.config_dir

    '''  configファイルの指定  '''
    #INI_FILE = '../config/system.ini'
    #pathname = os.path.dirname(os.path.abspath(__name__))
    #pathname = os.path.join(pathname, INI_FILE)
    #pathname = os.path.normpath(pathname)
    pathname = os.path.join(pathname, 'system.ini')
    config   = config_ini.ConfigIni()
    config.ini_file = pathname

    #現在の日時算出
    now_time = datetime.now().strftime("%Y/%m/%d %H:%M:%S  dash button pushed\n")

    #log_fileの値が必要
    log_file = config.get_config_value_bykey(
        'data_settings', 'TWEET_LOG')
    log_dir = cm.data_dir
    log_file = os.path.join(log_dir, log_file)
    #ログにボタンが押された日時を書きこむ
    write_log_file(log_file, now_time)

    #つぶやいた回数を取得する
    countstr = log_file_count(log_file)

    #tweet内容を作る
    tweet = TWEET.replace('{countstr}', countstr)
    '''  twitterオブジェクトの取得  '''
    msg = factory.MsgSimpleFactory()
    twitter = msg.create('twitter')
    twitter.consumer_key = config.get_config_value_bykey(
        'twitter_settings','CONSUMER_KEY')
    twitter.consumer_secret = config.get_config_value_bykey(
        'twitter_settings','CONSUMER_SECRET')
    twitter.access_token = config.get_config_value_bykey(
        'twitter_settings','ACCESS_TOKEN')
    twitter.access_token_secret = config.get_config_value_bykey(
        'twitter_settings','ACCESS_TOKEN_SECRET')
    twitter.set_to(config.get_config_value_bykey(
        'twitter_settings','API_URL'))
    twitter.set_body(tweet)
    #twitterに送る
    if twitter.send():
        print('tweet成功')
    else:
        print('tweet失敗')